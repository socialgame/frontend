import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

// Modules
import { ServicesModule } from '@services/services.module';
import { ExtraModule } from './extra/extra.module';
import { PagesModule } from './pages/pages.module';

// components
import { AppComponent } from './app.component';

// Routing
import { APP_ROUTES } from './app.routing';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    ServicesModule,
    ExtraModule,
    PagesModule,

    // Routing
    APP_ROUTES
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
