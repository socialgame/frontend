import { Routes, RouterModule } from '@angular/router';

// Pages
import { LoginComponent } from './extra/login/login.component';
import { RegisterComponent } from './extra/register/register.component';
import { NoFoundComponent } from './extra/no-found/no-found.component';

const appRoutes: Routes = [
  { path: 'login',    component: LoginComponent    },
  { path: 'registro', component: RegisterComponent },
  { path: '**',       component: NoFoundComponent    }
];
export const APP_ROUTES = RouterModule.forRoot(appRoutes);
