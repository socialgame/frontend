import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

// Modules
import { SharedModule } from '@shared/shared.module';

// Compoents
import { HomeComponent } from './home/home.component';
import { GamesComponent } from './games/games.component';
import { ProfileComponent } from './profile/profile.component';
import { ContactComponent } from './contact/contact.component';
import { HelpComponent } from './help/help.component';
import { FriendComponent } from './friend/friend.component';
import { PagesComponent } from './pages.component';

// Routing
import { PAGES_ROUTES } from './pages.routing';

@NgModule({
  declarations: [
    HomeComponent,
    GamesComponent,
    ProfileComponent,
    ContactComponent,
    HelpComponent,
    FriendComponent,
    PagesComponent
  ],
  imports: [
    CommonModule,
    SharedModule,

    // Routing
    PAGES_ROUTES
  ]
})
export class PagesModule { }
