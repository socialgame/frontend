import { Routes, RouterModule } from '@angular/router';

// Pages
import { PagesComponent } from './pages.component';
import { HomeComponent } from './home/home.component';
import { GamesComponent } from './games/games.component';
import { FriendComponent } from './friend/friend.component';
import { ProfileComponent } from './profile/profile.component';
import { ContactComponent } from './contact/contact.component';
import { HelpComponent } from './help/help.component';

const pagesRoutes: Routes = [
  {
    path: '',
    component: PagesComponent,
    children: [
      { path: 'inicio',   component: HomeComponent    },
      { path: 'juegos',   component: GamesComponent   },
      { path: 'amigos',   component: FriendComponent  },
      { path: 'perfil',   component: ProfileComponent },
      { path: 'contacto', component: ContactComponent },
      { path: 'ayuda',    component: HelpComponent    },
      { path: '', redirectTo: '/inicio', pathMatch: 'full' }
    ]
  }
];
export const PAGES_ROUTES = RouterModule.forRoot(pagesRoutes);
