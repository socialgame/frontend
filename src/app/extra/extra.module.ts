import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

// Components
import { LoginComponent } from './login/login.component';
import { NoFoundComponent } from './no-found/no-found.component';
import { RegisterComponent } from './register/register.component';



@NgModule({
  declarations: [
    LoginComponent,
    NoFoundComponent,
    RegisterComponent
  ],
  imports: [
    CommonModule
  ]
})
export class ExtraModule { }
