import { Component, OnInit } from '@angular/core';

// Services
import { SearchService } from '@services/shared/search.service';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss']
})
export class SearchComponent implements OnInit {

  constructor(public _search: SearchService) {}

  ngOnInit() {}
}
