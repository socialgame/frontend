import { Component, OnInit } from '@angular/core';

// Services
import { SidebarService } from '@services/shared/sidebar.service';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.scss']
})
export class MenuComponent implements OnInit {

  constructor(public _sidebar: SidebarService ) { }

  ngOnInit() {
  }

}
