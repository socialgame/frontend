
export interface Menu {
  icon: string;
  name: string;
  link: string;
  active: boolean;
}
