import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

// Services
import { SearchService } from './shared/search.service';
import { SidebarService } from './shared/sidebar.service';

@NgModule({
  declarations: [],
  imports: [
    CommonModule
  ],
  providers: [
    SearchService,
    SidebarService
  ]
})
export class ServicesModule { }
