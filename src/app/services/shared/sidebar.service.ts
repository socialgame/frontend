import { Injectable } from '@angular/core';

// Interfaces
import { Menu } from '@models/menu.interface';

// Data
import { MENU } from '@info/menu.data';

@Injectable({
  providedIn: 'root'
})
export class SidebarService {

  private status: string;
  private menu: Menu[];
  private selected: string;

  constructor() {
    this.menu = MENU;
  }

  // Retorna el estado del sidebar
  public isStatus() {
    return this.status;
  }

  // Abre y cierra el sidebar
  public toggle() {
    if (this.status === null || this.status === undefined) {
      this.status = 'show';
    } else {
      if (this.status === 'hidden') {
        this.status = 'show';
      } else {
        this.status = 'hidden';
      }
    }
  }

  // Marca como activa la pagina
  public classActive(link: string) {
    this.menu.forEach(
        item => (item.link === link) ? item.active = true : item.active = false
    );
  }

  // Retorna el menu
  public getMenu() {
    return this.menu;
  }

}
