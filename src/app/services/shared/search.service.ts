import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class SearchService {

  private status: boolean;

  constructor() {
    this.status = false;
  }

  // Retorna el estado del buscador
  public isStatus() {
    return this.status;
  }

  // Abre y cierra el buscador
  public toggle() {
    this.status = !this.status;
  }
}
