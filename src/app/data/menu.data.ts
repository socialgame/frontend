import { Menu } from '@models/menu.interface';

export const MENU: Menu[] = [
    { icon: 'fas fa-home',         name: 'Inicio',   link: '/inicio',   active: false },
    { icon: 'fas fa-gamepad',      name: 'Juegos',   link: '/juegos',   active: false },
    { icon: 'fas fa-user-friends', name: 'Amigos',   link: '/amigos',   active: false },
    { icon: 'fas fa-envelope',     name: 'Contacto', link: '/contacto', active: false },
    { icon: 'fas fa-info-circle',  name: 'Ayuda',    link: '/ayuda',    active: false }
];
