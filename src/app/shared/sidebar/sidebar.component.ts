import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

// Services
import { SidebarService } from '@services/shared/sidebar.service';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss']
})
export class SidebarComponent implements OnInit {

  constructor(public _sidebar: SidebarService, private router: Router) {
    this._sidebar.classActive(this.router.url);
  }

  ngOnInit() {
    this._sidebar.getMenu();
  }

  // Navega hacia la ruta y lo marca como active
  public async navigate(link: string) {
    await this.router.navigate([link])
      .then( () => {
        this._sidebar.classActive(link);
        this._sidebar.toggle();
      });
  }
}
